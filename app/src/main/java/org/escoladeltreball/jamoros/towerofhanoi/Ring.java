package org.escoladeltreball.jamoros.towerofhanoi;

import java.util.Stack;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;


public class Ring extends Sprite {

	private int mWeight; // representa el pes, importància, mida de l'anell i al
							// mateix temps és un valor únic
	private Stack<Ring> mStack; // representa la pila a la qual l'anell pertany
	private Sprite mTower; // l'sprite de la torre en la qual l'anell està
							// actualment col·locat.

	public Ring(int weight, float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.mWeight = weight;
	}

	public int getmWeight() {
		return mWeight;
	}

	public Stack<Ring> getmStack() {
		return mStack;
	}

	public void setmStack(Stack<Ring> mStack) {
		this.mStack = mStack;
	}

	public Sprite getmTower() {
		return mTower;
	}

	public void setmTower(Sprite mTower) {
		this.mTower = mTower;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Ring)) {
			return false;
		}
		return this.mWeight == ((Ring) obj).mWeight;
	}

	@Override
	public int hashCode() {
		return this.mWeight;
	}

}
/*
 * Recordeu que si volem definir quan 2 objectes són iguals hem de sobreescriure
 * el mètode equals que heretem de Object, com per exemple fem aqui.
 * I és obligatori sobreescriure llavors el mètode hashCode si no volem tenir inconsistències.
 * L'única condició perquè el mètode hashCode funcioni és que si 2 objectes són iguals retornin el mateix hashCode.
 * Això implica que un hashCode com aquest seria vàlid:
 * 	@Override
	public int hashCode() {
		return 666;
	}
 * 
 * però no serà tant eficient si a més fem que per a objectes diferents, retornin hashcodes diferents.
 * Per exemple:
 * 	@Override
	public int hashCode() {
		return this.mWeight;
	}
 * 
 */

package org.escoladeltreball.jamoros.towerofhanoi;

import android.util.Log;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.adt.io.in.IInputStreamOpener;
import org.andengine.util.debug.Debug;

import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;

public class TowerOfHanoiActivity extends SimpleBaseGameActivity {

    private static int CAMERA_WIDTH = 800;
    private static int CAMERA_HEIGHT = 480;
    private ITextureRegion mBackgroundTextureRegion, mTowerTextureRegion,
            mRing1TextureRegion, mRing2TextureRegion, mRing3TextureRegion;
    private Sprite mTower1, mTower2, mTower3;
    private Stack<Ring> mStack1, mStack2, mStack3;

    @Override
    public EngineOptions onCreateEngineOptions() {

        // Creem l'objecte Camera
        final Camera camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);

        // Creem l'objecte EngineOptions amb certs valors que
        // inclouen l'objecte Camera que acabem de crear, i retornem aquest
        // objecte EngineOptions
        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
                new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
    }

    @Override
    protected void onCreateResources() throws IOException {

        try {
            // 1 - Set up bitmap textures
            ITexture backgroundTexture = new BitmapTexture(
                    this.getTextureManager(), new IInputStreamOpener() {
                @Override
                public InputStream open() throws IOException {
                    return getAssets().open("gfx/background.png");
                }
            });
            ITexture towerTexture = new BitmapTexture(this.getTextureManager(),
                    new IInputStreamOpener() {
                        @Override
                        public InputStream open() throws IOException {
                            return getAssets().open("gfx/tower.png");
                        }
                    });
            ITexture ring1 = new BitmapTexture(this.getTextureManager(),
                    new IInputStreamOpener() {
                        @Override
                        public InputStream open() throws IOException {
                            return getAssets().open("gfx/ring1.png");
                        }
                    });
            ITexture ring2 = new BitmapTexture(this.getTextureManager(),
                    new IInputStreamOpener() {
                        @Override
                        public InputStream open() throws IOException {
                            return getAssets().open("gfx/ring2.png");
                        }
                    });
            ITexture ring3 = new BitmapTexture(this.getTextureManager(),
                    new IInputStreamOpener() {
                        @Override
                        public InputStream open() throws IOException {
                            return getAssets().open("gfx/ring3.png");
                        }
                    });
            // 2 - Load bitmap textures into VRAM
            backgroundTexture.load();
            towerTexture.load();
            ring1.load();
            ring2.load();
            ring3.load();

            // A continuació creem les TextureRegion's
            this.mBackgroundTextureRegion = TextureRegionFactory
                    .extractFromTexture(backgroundTexture);
            this.mTowerTextureRegion = TextureRegionFactory
                    .extractFromTexture(towerTexture);
            this.mRing1TextureRegion = TextureRegionFactory
                    .extractFromTexture(ring1);
            this.mRing2TextureRegion = TextureRegionFactory
                    .extractFromTexture(ring2);
            this.mRing3TextureRegion = TextureRegionFactory
                    .extractFromTexture(ring3);

        } catch (IOException e) {
            Debug.e(e);
        }
        // Finalment creem les piles -the stacks-
        this.mStack1 = new Stack<Ring>();
        this.mStack2 = new Stack<Ring>();
        this.mStack3 = new Stack<Ring>();
    }

    @Override
    protected Scene onCreateScene() {
        // Primer crearem l'objecte escena
        final Scene scene = new Scene();
        // Creem l'sprite de fons
        Sprite backgroundSprite = new Sprite(CAMERA_WIDTH / 2,
                CAMERA_HEIGHT / 2, this.mBackgroundTextureRegion,
                getVertexBufferObjectManager());
        // afegim l'sprite de fons a l'escena
        scene.attachChild(backgroundSprite);

        // Ara creem els sprites que representen les torres
        // tower.png: imatge 28 x 247 (Width x Height)

        mTower1 = new Sprite(192 + 28 / 2, CAMERA_HEIGHT - 63 - 247 / 2.0f,
                this.mTowerTextureRegion, getVertexBufferObjectManager());
        mTower2 = new Sprite(400 + 28 / 2, CAMERA_HEIGHT - 63 - 247 / 2.0f,
                this.mTowerTextureRegion, getVertexBufferObjectManager());
        mTower3 = new Sprite(604 + 28 / 2, CAMERA_HEIGHT - 63 - 247 / 2.0f,
                this.mTowerTextureRegion, getVertexBufferObjectManager());

        // Afegim els sprites a l'escena
        scene.attachChild(mTower1);
        scene.attachChild(mTower2);
        scene.attachChild(mTower3);

        // Versió anterior a anchor center
        // Ring ring1 = new Ring(1, 139, 174, this.mRing1TextureRegion,
        // getVertexBufferObjectManager());
        // Ring ring2 = new Ring(2, 118, 212, this.mRing2TextureRegion,
        // getVertexBufferObjectManager());
        // Ring ring3 = new Ring(3, 97, 255, this.mRing3TextureRegion,
        // getVertexBufferObjectManager());

        // Creem i afegim els anells a l'escena (tenint en compte un altre cop
        // que estem amb gles2 anchor center)

        // ring1.png: imatge 140 x 42 (Width x Height)
        Ring ring1 = new Ring(1, 139 + 140 / 2, CAMERA_HEIGHT - 174 - 42 / 2,
                this.mRing1TextureRegion, getVertexBufferObjectManager()) {

            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
                                         float pTouchAreaLocalX, float pTouchAreaLocalY) {
                // Si l'anell que està dalt de tot no és el que estem tocant no
                // fem res, retornant false
                if (((Ring) this.getmStack().peek()).getmWeight() != this
                        .getmWeight()) {
                    return false;
                }
                // En cas contrari agafem les coordenades del punt de la
                // pantalla on hem fet el "touch":
                // (pSceneTouchEvent.getX(), pSceneTouchEvent.getY())
                // i ho assignem al nostre anell

                this.setPosition(pSceneTouchEvent.getX(),
                        pSceneTouchEvent.getY());

                // Per comprovar/entendre la instrucció anterior, arrossega
                // l'anell al mateix temps que mires al Logcat com es van
                // escrivint els següents valors
                Log.d("ARROSSEGANT_L'ANELL", "les coordenades son: (" + pSceneTouchEvent.getX()
                        + ", " + pSceneTouchEvent.getY() + ")");
                // Si l'esdeveniment és ACTION_UP cridem al mètode checkForCollisionsWithTowers
                if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
                    checkForCollisionsWithTowers(this);
                }
                return true;
            }
        };

        // ring2.png: imatge 178 x 47 (Width x Height)
        Ring ring2 = new Ring(2, 118 + 178 / 2,
                CAMERA_HEIGHT - 212 - 47 / 2.0f, this.mRing2TextureRegion,
                getVertexBufferObjectManager()) {
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
                                         float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (((Ring) this.getmStack().peek()).getmWeight() != this
                        .getmWeight()) {
                    return false;
                }
                this.setPosition(pSceneTouchEvent.getX(),
                        pSceneTouchEvent.getY());
                if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
                    checkForCollisionsWithTowers(this);
                }
                return true;
            }
        };

        // ring3.png: imatge 223 x 58 (Width x Height)
        Ring ring3 = new Ring(3, 97 + 223 / 2.0f, CAMERA_HEIGHT - 255 - 58 / 2,
                this.mRing3TextureRegion, getVertexBufferObjectManager()) {
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
                                         float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (((Ring) this.getmStack().peek()).getmWeight() != this
                        .getmWeight()) {
                    return false;
                }
                this.setPosition(pSceneTouchEvent.getX(),
                        pSceneTouchEvent.getY());
                if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_UP) {
                    checkForCollisionsWithTowers(this);
                }
                return true;
            }
        };

        scene.attachChild(ring1);
        scene.attachChild(ring2);
        scene.attachChild(ring3);

        // Quan comenci el joc volem que tots els anells estiguin a la primera pila
        // i en un determinat ordre (de major a menor, amb l'anell més petit «on top»)
        // Afegim els anells a la primera pila
        this.mStack1.add(ring3);
        this.mStack1.add(ring2);
        this.mStack1.add(ring1);

        // Inicialitzem la variable pila de cada anell
        ring1.setmStack(mStack1);
        ring2.setmStack(mStack1);
        ring3.setmStack(mStack1);

        // Inicialitzem la variable Sprite de la torre de cada anell
        ring1.setmTower(mTower1);
        ring2.setmTower(mTower1);
        ring3.setmTower(mTower1);

        // Afegim els manegadors -handlers- d'esdevemiments tipus touch:
        // isActionUp, isActionCancel, isActionDown, isActionMove,
        // isActionOutside
        scene.registerTouchArea(ring1);
        scene.registerTouchArea(ring2);
        scene.registerTouchArea(ring3);

        // Aquest mètode l'afegim per evitar situacions tipus (*)
        scene.setTouchAreaBindingOnActionDownEnabled(true);

        return scene;
    }

    private void checkForCollisionsWithTowers(Ring ring) {
        Stack<Ring> stack = null;
        Sprite tower = null;

        // Comprovem si l'anell col·lisiona amb la 'torre 1' i al mateix temps
        // està buida o, el que és el mateix,
        // la pila 1 que és la pila associada a aquesta torre està buida o
        // l'anell que estic movent té menys pes -weight- que el que es troba a
        // dalt de tot de la pila.
        if (ring.collidesWith(mTower1)
                && (mStack1.size() == 0 || ring.getmWeight() < ((Ring) mStack1
                .peek()).getmWeight())) {
            // Si hi ha col·lisió amb la torre 1 amb les condicions permesses
            // pel joc,
            // posem que la pila i torre són aquesta
            stack = mStack1;
            tower = mTower1;

            // Fem el mateix per les altres torres
        } else if (ring.collidesWith(mTower2)
                && (mStack2.size() == 0 || ring.getmWeight() < ((Ring) mStack2
                .peek()).getmWeight())) {
            stack = mStack2;
            tower = mTower2;
        } else if (ring.collidesWith(mTower3)
                && (mStack3.size() == 0 || ring.getmWeight() < ((Ring) mStack3
                .peek()).getmWeight())) {
            stack = mStack3;
            tower = mTower3;
        } else {
            stack = ring.getmStack();
            tower = ring.getmTower();
        }
        // Eliminem l'anell que està a dalt de tot, fem pop. <---No m'agrada
        // molt això
        ring.getmStack().remove(ring);

        // Les coordenades horitzontals, x, de l'anell coincidiran amb els de la
        // torre, però no l'alçada.
        // Diferenciem si la torre està buida o no.

        // Si la torre està buida, la coordenada y de l'anell serà la de la
        // torre
        // (aquí hem trobaria al centre de la torre) menys la meitat de la
        // alçada -height- de la torre (aquí hem trobaria a la base) més la
        // meitat de
        // l'alçada de l'anell que estic movent
        if (stack != null && tower != null && stack.size() == 0) {
            ring.setPosition(tower.getX(), tower.getY() - tower.getHeight()
                    / 2.0f + ring.getHeight() / 2.0f);
        }
        // Si la torre no està buida, la coordenada y de l'anell serà la de
        // l'anell que estigui a dalt de tot més la meitat de l'alçada de
        // l'anell que estic movent.
        else if (stack != null && tower != null && stack.size() > 0) {
            ring.setPosition(tower.getX(),
                    (((Ring) stack.peek()).getY() + ring.getHeight()));
        }
        // Afegeixo a la pila l'anell
        stack.add(ring);
        ring.setmStack(stack);
        ring.setmTower(tower);
    }
}

/*
 * Si es vol veure on està el anchor center d'un Sprite podem utilitzar el Log.d
 * per recuperar les coordenades getOffsetCenterX() i getOffsetCenterY(). Una
 * altra manera de fer-ho, més llarga, però amb la qual apendrem més conceptes
 * és afegint un objecte Text a l'escena. Per a això afegirem el següent codi a
 * onCreateScene:
 *
 * font = (FontFactory.create(this.getFontManager(), this.getTextureManager(),
 * 256, 256, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32));
 * font.load();
 *
 * Text text = new Text(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2, font ,
 * "Anchor Center: " + backgroundSprite.getOffsetCenterX() + "," +
 * backgroundSprite.getOffsetCenterY() , getVertexBufferObjectManager());
 * scene.attachChild(text);
 *
 *
 * D'aquesta manera veurem que l'anchor center és 0.5, 0.5 de manera que les
 * nostres coordenades de la creació de l'Sprite hauran de fer referència al
 * centre de l'sprite i no a una cantonada.
 */

/*
 * Potser estaria millor dissenyar una classe Torre amb les variables
 * d'instància Sprite i Stack
 */

/*
 * (*) Imaginem que estem desenvolupant un joc de carreres i sobreescrivim el
 * mètode onAreaTouched de manera que si premo el botó (action down) accelerem i
 * si aixeco el dit del botó (action up) freno. Que passaria si premo el botó i
 * arrossego el dit per la pantalla fins que surto de l'àrea del botó ? Doncs
 * que no es cridaria l'acció corresponent a action up i per tant es continuaria
 * accelerant. Això és el que evita aquest mètode.
 * http://stackoverflow.com/questions
 * /19754670/what-is-the-purpose-of-settouchareabindingonactiondownenabled
 * -andengine/19754995#19754995
 */

/*
 * Millores: checkForCollisionsWithTowers millor com a mètode de instància de la
 * classe Ring i que no rebi cap argument i de fet el nom és millorable, ja que
 * d'aquesta manera sembla que hagi de retornar un booleà.
 *
 * El mètode tampoc m'agrada molt, en el sentit de que treu de la pila l'anell
 * que estem movent SEMPRE, i després ja tornarà a posar-lo al mateix lloc si no
 * hi ha una col·lisió adient. Millor buscar una estructura if i moure de la
 * pila NOMÉS quan és necessari.
 */

/*
 * Millores: Utilitzar el mètode equals en onAreaTouched en comptes de mirar
 * directament el pes
 */


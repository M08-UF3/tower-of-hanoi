LICENSE
=======

This is free software, you can redistribute it and/or modify it under the terms of the GNU General Public License v3.

See this [link](http://www.gnu.org/licenses/gpl.html) for a more exhaustive information.

